
		<div class="b-footer">

		<?php dynamic_sidebar( 'footer-1' ); ?>

	            <nav class="b-footer__main__col__menu">
	              <div class="b-footer__main__col__menu__i"><a href="#" class="b-footer__main__col__menu__i__link">אודות מילסטון</a></div><span>/</span>
	              <!--div(class=name+'__main__col__menu__i')-->
	              <!--    a(class=name+'__main__col__menu__i__link', href='#') אולמות תצוגה-->
	              <!--span /-->
	              <!--div(class=name+'__main__col__menu__i')-->
	              <!--    a(class=name+'__main__col__menu__i__link', href='#') חדשנות והשראה-->
	              <!--span /-->
	              <div class="b-footer__main__col__menu__i"><a href="#" class="b-footer__main__col__menu__i__link">הקולקציה שלנו</a></div>
	            </nav>
	          </div>
	          <div class="b-footer__main__col">
	            <div class="b-footer__main__col__links">
	              <p class="b-footer__main__col__text"><?php echo get_option( 'copyright', '' ) ?></p>
	              <div class="b-footer__main__col__links__box">
	                <div class="b-footer__main__col__links__box__i"><a href="#">מידע משפטי</a></div><span>/</span>
	                <div class="b-footer__main__col__links__box__i"><a href="#">הסכם פרטיות</a></div>
	              </div>
	            </div>
	            <p class="b-footer__main__col__copyright">Website by&nbsp;<a href="/" class="b-footer__main__col__copyright__link">WebScience</a></p>
	          </div>
	        </div>
	        <div class="b-footer__favorites"></div>
	      </div>
	
	<?php wp_footer(); ?>
</body>
</html>