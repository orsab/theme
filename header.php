<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

  <?php wp_head(); ?>

	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1"/>
  <link href="http://fonts.googleapis.com/earlyaccess/opensanshebrew.css" type="text/css" rel="stylesheet"/>
</head>

<body class="heb" <?php body_class(); ?>>
	<div id="page" class="hfeed site b-container _index">
		<header class="b-header">
        <div class="b-header__main"><a href="index.html" class="b-header__main__logo">
            <svg version="1.1" viewbox="0 0 500 72" class="b-header__main__logo__i">
              <path d="M23.8,4.2l21.6,47.9L66.9,4.2h19.3v1.2h-1.6c-2.5,0-4.4,0.5-5.7,1.5c-1.3,1-2,2.6-2,5v48.5c0,2.4,0.7,4,2,5                    c1.3,1,3.2,1.5,5.7,1.5h1.6V68H57.5v-1.2h1.6c5.1,0,7.6-2.2,7.6-6.5V14.3L42.7,68h-0.8l-24-53.9v46.2c0,4.3,2.5,6.5,7.6,6.5h1.6V68                    H4.5v-1.2H6c5.1,0,7.6-2.2,7.6-6.5V11.8c0-1.7-0.4-3.2-1.3-4.5s-3-2-6.3-2H4.5V4.2H23.8z"></path>&#x9;
              <path d="M93.5,4.2h28.6v1.2h-1.6c-3,0-5.1,0.6-6.2,1.8c-1.1,1.2-1.6,2.7-1.6,4.7v48.5c0,2,0.5,3.5,1.6,4.7c1.1,1.2,3.1,1.8,6.2,1.8                    h1.6V68H93.5v-1.2H95c5.1,0,7.6-2.2,7.6-6.5V11.8c0-1.7-0.4-3.2-1.3-4.5s-3-2-6.3-2h-1.5V4.2z"></path>&#x9;
              <path d="M148.7,63.7h17.2c8.3,0,14.1-4.3,17.3-12.8l1.1,0.4L179.1,68h-49.8v-1.2h1.5c5.1,0,7.6-2.2,7.6-6.5V11.8                    c0-1.7-0.4-3.2-1.3-4.5s-3-2-6.3-2h-1.5V4.2h28.6v1.2h-1.6c-2.3,0-4.1,0.4-5.2,1.2c-1.1,0.7-1.8,1.6-2.1,2.5                    c-0.3,0.9-0.4,1.8-0.4,2.9V63.7z"></path>&#x9;
              <path d="M226.4,3.4v20.5h-1.2c-0.1-3-0.9-5.7-2.4-8.3c-1.5-2.6-3.5-4.6-6-6.2c-2.5-1.5-5.1-2.3-8-2.3c-2,0-3.7,0.4-5.3,1.2                    c-1.6,0.8-2.8,1.9-3.7,3.2c-0.9,1.4-1.3,2.8-1.3,4.3c0,1.7,0.5,3.4,1.5,4.9c1,1.5,2.3,2.9,3.9,4.2c1.6,1.2,3.4,2.5,5.5,3.7                    c2.1,1.2,4.1,2.3,5.9,3.4c3.4,2,6.1,4,8.3,6c2.2,2,3.8,4.2,5,6.7c1.1,2.4,1.7,5.1,1.7,8.1c0,3.5-1,6.5-3,8.9s-4.5,4.2-7.7,5.4                    c-3.1,1.2-6.4,1.8-9.7,1.8c-2,0-3.6-0.2-4.9-0.5c-1.3-0.4-2.8-0.9-4.7-1.6c-1.9-0.7-3.3-1.1-4.4-1.2h-1.5c-1.7,0.3-2.7,1.4-3.1,3.3                    h-1.2l0.1-21.6h1.2c0.8,4.9,2.6,9,5.6,12.5c3,3.4,6.8,5.2,11.6,5.2c3.5,0,6.4-0.8,8.7-2.5c2.3-1.7,3.5-4,3.5-7                    c0-2.5-0.7-4.8-2.1-6.8c-1.4-2-3.1-3.7-5-5.1c-2-1.4-4.5-3-7.7-4.8c-3.4-1.9-6.2-3.7-8.4-5.3c-2.2-1.6-4-3.6-5.5-5.9                    c-1.4-2.3-2.1-4.9-2.1-7.9c0-3.2,0.8-6,2.5-8.5c1.7-2.5,3.9-4.4,6.8-5.7c2.8-1.3,5.9-2,9.2-2c2.2,0,4,0.2,5.5,0.6                    c1.4,0.4,2.9,0.9,4.6,1.5c1.6,0.6,2.8,1,3.5,1c0.7,0,1.4-0.3,2-0.9c0.6-0.6,1-1.3,1.1-2.2H226.4z"></path>&#x9;
              <path d="M237.8,4.2h54.3l0.8,14.4h-1.2c-0.2-2.9-1.1-5.3-2.9-7.2c-1.7-1.9-4.6-2.9-8.7-2.9h-10.1v51.8c0,4.3,2.5,6.5,7.6,6.5h1.6                    V68h-28.6v-1.2h1.6c2.5,0,4.4-0.5,5.7-1.5c1.3-1,2-2.6,2-5V8.5h-10.2c-4,0-6.9,1-8.7,2.9c-1.7,1.9-2.7,4.3-2.9,7.2H237L237.8,4.2z"></path>&#x9;
              <path d="M328,68.9c-5.7,0-10.8-1.5-15.3-4.5c-4.5-3-8.1-7-10.6-12.1c-2.5-5.1-3.8-10.6-3.8-16.5c0-5.2,0.9-9.8,2.8-14                    c1.8-4.1,4.3-7.6,7.4-10.3c3.1-2.7,6.5-4.8,10.2-6.2c3.7-1.4,7.4-2.1,11.2-2.1c5.7,0,10.8,1.4,15.3,4.3s8.1,6.9,10.6,11.9                    c2.5,5.1,3.8,10.6,3.8,16.7c0,4.8-0.8,9.2-2.4,13.2c-1.6,4-3.9,7.5-6.8,10.4c-2.9,2.9-6.3,5.1-10.1,6.6                    C336.3,68.1,332.3,68.9,328,68.9z M328.5,65.1c2.5,0,5-0.6,7.4-1.8c2.4-1.1,4.5-2.9,6.3-5.2c1.9-2.4,3.4-5.4,4.5-9                    c1.1-3.7,1.6-7.9,1.6-12.7c0-5.3-0.6-9.8-1.8-13.7c-1.1-3.8-2.7-6.9-4.5-9.1c-1.9-2.3-4-3.9-6.2-5c-2.2-1-4.4-1.5-6.6-1.5                    c-3.4,0-6.6,1-9.6,3.1c-3,2.1-5.5,5.3-7.4,9.6c-1.9,4.3-2.8,9.6-2.8,16c0,4.3,0.5,8.3,1.4,11.9c0.9,3.6,2.2,6.7,3.9,9.4                    c1.7,2.6,3.7,4.6,6.1,6S325.7,65.1,328.5,65.1z"></path>&#x9;
              <path d="M383.6,4.2l39.6,46.6v-39c0-4.3-2.5-6.4-7.6-6.4H414V4.2h22.5v1.2h-1.6c-5.1,0-7.6,2.2-7.6,6.5v57.1h-1.2l-44.8-52.1v43.5                    c0,2.4,0.7,4,2,5c1.3,1,3.2,1.5,5.7,1.5h1.6V68h-22.5v-1.2h1.6c5.1,0,7.6-2.2,7.6-6.5V11.9c-1.7-2.2-3.6-3.8-5.5-4.9                    c-2-1.1-4.2-1.6-6.8-1.6V4.2H383.6z"></path>&#x9;
              <path d="M491.8,4.2l0.8,13.3l-1.3,0.2c-0.9-3.8-2.2-6.3-3.9-7.4c-1.7-1.1-4.1-1.7-7.2-1.7h-17.6v23.9h16.4c1.3,0,2.5-0.2,3.5-0.6                    c1-0.4,1.8-1.3,2.6-2.7c0.7-1.4,1.1-3.5,1.1-6.2h1.2v22.3h-1.2c-0.1-2.5-0.5-4.4-1.1-5.7c-0.7-1.3-1.5-2.2-2.5-2.6                    c-1-0.4-2.2-0.6-3.5-0.6h-16.4v27.4h17.6c4.5,0,8-0.9,10.6-2.6c2.5-1.7,4.6-4.8,6.2-9.3l1.1,0.3L493.3,68h-50.1v-1.2h1.5                    c5.1,0,7.6-2.2,7.6-6.5V11.8c0-1.7-0.4-3.2-1.3-4.5s-3-2-6.3-2h-1.5V4.2H491.8z"></path>
            </svg></a>
          <div class="b-header__main__lang">
            <div class="b-header__main__lang__itm">
              <div class="b-header__main__lang__itm__select"><a href="#" class="b-header__main__lang__itm__select__link _heb">קטלוגים</a><span>/</span></div>
              <div class="b-header__main__lang__itm__select"><a href="#" class="b-header__main__lang__itm__select__link _heb">הבלוג שלנו</a><span>/</span></div>
              <div class="b-header__main__lang__itm__select"><a href="index.html" class="b-header__main__lang__itm__select__link _eng">ENGLISH</a></div>
            </div>
          </div>
          <nav class="b-header__main__menu">
            <div class="b-header__main__menu__box">
              <div class="b-header__main__menu__box__rasp">
                <div class="b-header__main__menu__box__i"><a href="#" class="b-header__main__menu__box__i__link">אודות מילסטון<span></span></a>
                  <div class="b-header__main__menu__box__i__pod-menu _right">
                    <div class="b-header__main__menu__box__i__pod-menu__box"><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s7.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>פסיפסים</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s6.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>שטיחים</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s4.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>ריצופי אבן</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s5.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>בריקים</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s3.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>קרמיקה</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s2.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>אביזרי אמבט</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/s1.jpg)" href="category.html" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>קרניזים ופנלים</span></div></a></div>
                  </div>
                </div>
                <div class="b-header__main__menu__box__i"><a href="#" class="b-header__main__menu__box__i__link">אולמות תצוגה</a></div>
                <div class="b-header__main__menu__box__i"><a href="#" class="b-header__main__menu__box__i__link">חדשנות והשראה</a></div>
                <div class="b-header__main__menu__box__i"><a href="#" class="b-header__main__menu__box__i__link">הקולקציה שלנו<span></span></a>
                  <div class="b-header__main__menu__box__i__pod-menu _left">
                    <div class="b-header__main__menu__box__i__pod-menu__box"><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/5.jpg)" href="#" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>עלינו</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/4.jpg)" href="#" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>מוזיאון</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/3.jpg)" href="#" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>לחץ</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/2.jpg)" href="#" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>תערוכה</span></div></a><a style="background-image: url(<?=get_template_directory_uri()?>/assets/img/1.jpg)" href="#" class="b-header__main__menu__box__i__pod-menu__box__i">
                        <div class="b-header__main__menu__box__i__pod-menu__box__i__name"><span>צור קשר</span></div></a></div>
                  </div>
                </div>
              </div><span class="b-header__main__menu__box__modal b-modal"></span>
            </div>
          </nav>
          <div class="b-header__main__search">
            <div class="b-header__main__search__modal b-modal">
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="b-header__main__search__modal__box">
                <div class="b-header__main__search__modal__box__desc">
                  <p class="b-header__main__search__modal__box__desc__title">פסיפס פנינה סיבירית</p>
                  <p class="b-header__main__search__modal__box__desc__model">MIL23291032</p>
                  <div class="b-header__main__search__modal__box__desc__heart b-heart">
                    <svg version="1.1" viewBox="0 0 36 35">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>
                    </svg>
                  </div>
                </div>
              </div>
            </div>
            <div class="b-header__main__search__box">
              <form>
                <input type="text" class="b-header__main__search__box__form"/>
                <svg version="1.1" viewBox="0 0 16.5 17.2" class="b-header__main__search__box__ico">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M5.8,12.9l-3.6,3.6c-0.5,0.5-1.3,0.5-1.8,0c-0.5-0.5-0.5-1.3,0-1.8L4,11.1c0.4-0.4,0.9-0.5,1.3-0.3c-0.9-1.1-1.5-2.4-1.5-4c0-3.4,2.7-6.1,6.1-6.1c3.4,0,6.1,2.7,6.1,6.1c0,3.4-2.7,6.1-6.1,6.1c-1.5,0-2.8-0.5-3.9-1.4C6.3,12,6.2,12.6,5.8,12.9z M14.5,6.9c0-2.5-2-4.5-4.5-4.5c-2.5,0-4.5,2-4.5,4.5c0,2.5,2,4.5,4.5,4.5C12.5,11.3,14.5,9.3,14.5,6.9z"></path>
                </svg>
              </form>
            </div>
          </div>
        </div>
      </header>
