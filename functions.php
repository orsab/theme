<?php
// @author: Or Alkin
// @since: 25/09/2015
// 
add_action( 'wp_enqueue_scripts', 'oa_styles_scripts' );
define("THEME_PATH", get_template_directory_uri());

function oa_styles_scripts(){

	// Register Styles
	wp_register_style( 'oa-project-style', THEME_PATH.'/assets/css/project.min.css');

	// Enqueue Styles
	wp_enqueue_style( 'oa-project-style' );

	// Register scripts
	wp_register_script( 'oa-modernizr-js', 			THEME_PATH.'/assets/js/vendor/modernizr.js');
	wp_register_script( 'oa-jquery-js', 			THEME_PATH.'/assets/js/vendor/jquery-1.10.2.min.js');
	wp_register_script( 'oa-jquery.mobile-js', 		THEME_PATH.'/assets/js/vendor/jquery.mobile.custom.min.js');
	wp_register_script( 'oa-jquery.touchSwipe-js', 	THEME_PATH.'/assets/js/vendor/jquery.touchSwipe.js');
	wp_register_script( 'oa-masonry.pkgd-js', 		THEME_PATH.'/assets/js/vendor/masonry.pkgd.min.js');
	wp_register_script( 'oa-project-js', 			THEME_PATH.'/assets/js/project.js', [],'1.0', true);
	// Enque scripts
	wp_enqueue_script( 'oa-modernizr-js' );
	wp_enqueue_script( 'oa-jquery-js' );
	wp_enqueue_script( 'oa-jquery.mobile-js' );
	wp_enqueue_script( 'oa-jquery.touchSwipe-js' );
	wp_enqueue_script( 'oa-masonry.pkgd-js' );
	wp_enqueue_script( 'oa-project-js' );
}

//**
// Gallery Shortcode, usage like in standard gallery of WP( [gallsery ids=1,2,3,4] )
remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'oa_gallery' );
function oa_gallery($atts){

	$img_id = explode(',', $atts['ids']);
	if( !$img_id[0] ) return;

	$html = '<div class="b-slider">';
	$html .= '<div class="b-slider__main">';
	$active = '_act';
	foreach($img_id as $item) {
		$img_data = get_posts( [
			'p' => $item,
			'post_type' => 'attachment'
		] );

		// Full file path
		$img_full = wp_get_attachment_image_src( $item, 'full' );

		$html .= "<img src={$img_full[0]} class=\"b-slider__main__i {$active}\">";
		$active = '';
	}

	$html .= '</div>';
	$html .= '<div class="b-slider__arrow _left"></div>';
	$html .= '<div class="b-slider__arrow _right"></div>';
	$html .= '</div>';
// echo "<script>console.log(".json_encode($product).");</script>";
	return $html;
}

// Copyright handler, added field into General settings
add_filter('admin_init', 'my_copyright');
function my_copyright() {
    register_setting('general', 'copyright', 'esc_attr');
    add_settings_field('copyright', '<label for="copyright">'.__('Copyright' , 'copyright' ).'</label>' , 'my_copyright_html', 'general');
}
function my_copyright_html() {
    $value = get_option( 'copyright', '' );
    echo '<input type="text" id="copyright" name="copyright" value="' . $value . '" />';
}

// Registering Widget for Social Icons
function oa_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Footer', 'oa-theme' ),
		'id'            => 'footer-1',
		'description'   => __( 'Appears in the footer section of the site.', 'oa-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'oa_theme_widgets_init' );

class OA_Widget extends WP_Widget{

	function __construct(){
		$args = [
			'name' => 'Footer Icons',
			'description' => 'Social Icons',
			'class' => 'oa-widget'
		];
		parent::__construct('oa-widget', '', $args);
	}

	function widget($args, $instance){
		$icons = explode(',', $instance['icons']);
		if(count($icons) == 0)
			return;

		$template = '<a href="#" class="b-footer__main__col__soc__i">
						<svg version="1.1" viewBox="0 0 22 22">
	                  		<path d="{icon}"></path>
	                	</svg>
	                </a>';

		echo '<div class="b-footer__main">
	          <div class="b-footer__main__col">
	            <div class="b-footer__main__col__soc">';
		foreach ($icons as $icon) {
			switch ($icon) {
				case 'inst':
					echo str_replace('{icon}', 'M15.6,8.6v-2l-0.3,0l-1.8,0l0,2L15.6,8.6z M11,13.2c1.2,0,2.1-0.9,2.1-2.1c0-0.5-0.1-0.9-0.4-1.2C12.3,9.4,11.7,9,11,9S9.7,9.4,9.3,9.9c-0.2,0.3-0.4,0.8-0.4,1.2C8.9,12.3,9.8,13.2,11,13.2z M11,0c-6,0-11,4.9-11,11c0,6,4.9,11,11,11c6,0,11-4.9,11-11C22,4.9,17.1,0,11,0z M16.9,14.8c0,1.3-1,2.3-2.3,2.3H7.3c-1.3,0-2.3-1-2.3-2.3V7.5c0-1.3,1-2.3,2.3-2.3h7.3c1.3,0,2.3,1,2.3,2.3L16.9,14.8L16.9,14.8z M14.3,11.1c0,1.8-1.5,3.3-3.3,3.3c-1.8,0-3.3-1.5-3.3-3.3c0-0.4,0.1-0.8,0.2-1.2H6.2v4.9c0,0.6,0.5,1.1,1.1,1.1h7.3c0.6,0,1.1-0.5,1.1-1.1V9.9H14C14.2,10.3,14.3,10.7,14.3,11.1z', $template);
					break;
				case 'g+':
					echo str_replace('{icon}', 'M11,0C4.9,0,0,4.9,0,11s4.9,11,11,11c6.1,0,11-4.9,11-11S17.1,0,11,0z M6.2,16.9v-5.3l4.6,2.6L6.2,16.9z M10.7,13.7l-4.6-2.6l4.6-2.7V13.7z M10.7,7.9l-4.6,2.7V5.2l4.6-2.7V7.9z M15.8,16.8l-4.6,2.7v-5.3l4.6-2.7V16.8z M11.3,13.7V8.4l4.6,2.6L11.3,13.7z M15.8,10.5l-4.6-2.6l4.6-2.7V10.5z', $template);
					break;
				case 'face':
					echo str_replace('{icon}', 'M11,0C4.9,0,0,4.9,0,11c0,6.1,4.9,11,11,11s11-4.9,11-11C22,4.9,17.1,0,11,0z M14,17.4c-0.5,0-3-0.1-3.6-0.1s-4-1-4-4.7c0-0.7,0-1.3,0-2c0-0.6,0-1.2,0-1.8c0-0.2,0-0.4,0-0.5c0-0.9-0.1-2,0.2-2.9C6.8,4.8,7.3,4.5,8,4.5c1.6,0,1.7,1.6,1.7,3.3h4.6c0.7,0,1.3,0.7,1.3,1.9s-1.2,1.4-1.8,1.4H9.7c0,0.7,0,1.3,0,1.6c0,0.7,0.3,1.5,2.1,1.5h2.5c0.6,0,1.5,0.5,1.5,1.8C15.7,17.3,14.4,17.4,14,17.4z', $template);
					break;
				case 'twit':
					echo str_replace('{icon}', 'M11,0C4.9,0,0,4.9,0,10.9c0,6.1,4.9,11,11,11s11-4.9,11-11S17,0,11,0z M13.9,10.9H12c0,3,0,6.8,0,6.8H9.2c0,0,0-3.7,0-6.8H7.8V8.5h1.3V7c0-1.1,0.5-2.8,2.8-2.8l2.1,0v2.3c0,0-1.3,0-1.5,0S12,6.6,12,7.1v1.4h2.1L13.9,10.9z', $template);
					break;
				case 'i1':
					echo str_replace('{icon}', 'M5.4,15.7c-0.5-0.5-0.9-1.1-0.9-1.9c0-0.4,0.1-0.9,0.3-1.3c-0.2,0-0.4,0-0.6,0c-1.6,0-2.9-0.6-3.9-1.5c0,1.8,0.5,3.5,1.4,5c0.8-0.2,1.6-0.4,2.5-0.4C4.6,15.7,5,15.7,5.4,15.7z M5.2,11.9c1.7,0.1,2.9-1.8,2.6-4s-2-4.1-3.7-4.2C3.5,3.7,3,3.9,2.6,4.2C2.2,4.6,1.9,5.1,1.6,5.6c-0.2,0.6-0.3,1.3-0.2,2C1.8,9.9,3.4,11.8,5.2,11.9z M11.1,0C8.3,0,5.8,1,3.9,2.8c0.3-0.1,0.7-0.1,1-0.1c1.7,0,6.7,0,6.7,0l-1.5,0.9H8c1.4,0.4,2.2,2.4,2.2,4.1c0,1.4-0.8,2.8-1.9,3.7C7.2,12.1,7,12.4,7,13.2c0,0.6,1.2,1.7,1.8,2.1c1.8,1.3,2.4,2.4,2.4,4.4c0,0.6-0.1,1.2-0.4,1.7c0,0,0,0,0,0c-0.5,0-1,0-1.5-0.1c0.2-0.4,0.2-0.9,0.2-1.4c0-0.2,0-0.5-0.1-0.7C9,18.1,8,17.6,6.6,16.6c-0.5-0.2-1.1-0.3-1.7-0.3c-1,0-1.9,0.2-2.7,0.6c1.9,2.8,5.2,4.6,8.8,4.6c5.9,0,10.8-4.8,10.8-10.8C21.8,4.8,17,0,11.1,0z M16.1,11.8v3.5h-1.7v-3.5h-3.9V10h3.9V6.1h1.7V10H20v1.7H16.1z', $template);
					break;
				case 'i2':
					echo str_replace('{icon}', 'M11,0C5,0,0.1,4.9,0.1,10.9c0,4.5,2.7,8.3,6.5,10c0-0.8,0-1.7,0.2-2.5c0.2-0.9,1.4-6,1.4-6s-0.3-0.7-0.3-1.7c0-1.6,0.9-2.8,2.1-2.8c1,0,1.5,0.7,1.5,1.6c0,1-0.6,2.5-1,3.9c-0.3,1.2,0.6,2.1,1.7,2.1c2.1,0,3.5-2.7,3.5-5.8c0-2.4-1.6-4.2-4.6-4.2c-3.3,0-5.4,2.5-5.4,5.2c0,1,0.3,1.6,0.7,2.1c0.2,0.2,0.2,0.3,0.2,0.6c-0.1,0.2-0.2,0.7-0.2,0.9c-0.1,0.3-0.3,0.4-0.5,0.3c-1.5-0.6-2.2-2.3-2.2-4.2c0-3.1,2.6-6.8,7.8-6.8c4.2,0,6.9,3,6.9,6.3c0,4.3-2.4,7.5-5.9,7.5c-1.2,0-2.3-0.6-2.7-1.4c0,0-0.6,2.5-0.8,3c-0.2,0.8-0.7,1.7-1.1,2.3c1,0.3,2,0.4,3.1,0.4c6,0,10.9-4.9,10.9-10.9C21.9,4.9,17,0,11,0z', $template);
					break;

				default:
					# code...
					break;
			}
		}

	    echo '</div>';
	}

	function form($instance){
		if ( isset( $instance[ 'icons' ] ) ) {
			$icons = $instance[ 'icons' ];
		}
		else {}

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'icons' ); ?>"><?php _e( 'Icons:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'icons' ); ?>" name="<?php echo $this->get_field_name( 'icons' ); ?>" type="text" value="<?php echo esc_attr( $icons ); ?>" />
		</p>
		<?php
	}
}

function oa_load_widget(){
	register_widget( 'OA_Widget' );
}
add_action( 'widgets_init', 'oa_load_widget' );



/** Update Add Custom Post Type
*   @since 10/01/2015
*	@author Or Alkin
*/
add_theme_support( 'post-thumbnails' );
// Register Custom Post Type
function products_post_type() {

	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Product', 'text_domain' ),
		'name_admin_bar'      => __( 'Post Type', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Product:', 'text_domain' ),
		'all_items'           => __( 'All Products', 'text_domain' ),
		'add_new_item'        => __( 'Add New Product', 'text_domain' ),
		'add_new'             => __( 'New Product', 'text_domain' ),
		'new_item'            => __( 'New Item', 'text_domain' ),
		'edit_item'           => __( 'Edit Product', 'text_domain' ),
		'update_item'         => __( 'Update Product', 'text_domain' ),
		'view_item'           => __( 'View Product', 'text_domain' ),
		'search_items'        => __( 'Search products', 'text_domain' ),
		'not_found'           => __( 'No products found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No products found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Product', 'text_domain' ),
		'description'         => __( 'Product information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'products_post_type', 0 );