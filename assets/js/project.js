if(Modernizr.touch){
    $('body').addClass('_mobile')
}

$('.b-header__main__menu__box__i__link').on('click', function(){
    if(Modernizr.touch || $(window).width() <= 640 && $(this).next().length){
        var selectMenu = $(this);

        if(selectMenu.hasClass('_act')){
            selectMenu.removeClass('_act').next().slideUp('normal');
        } else {
            if($('.b-header__main__menu__box__i__link').hasClass('_act')){
                $('.b-header__main__menu__box__i__link._act').removeClass('_act').next().slideUp('normal', function(){
                    selectMenu.addClass('_act').next().slideDown('normal');
                });
            } else {
                selectMenu.addClass('_act').next().slideDown('normal');
            }
        }
    }
});


$('.b-heart').on('click', function(){
    $(this).toggleClass('_act');
});

$('.b-header__main__menu').on('click touch', function(e){
    var target = $(e.target);
    if(Modernizr.touch || $(window).width() <= 640){
        if(!target.parents().hasClass('b-header__main__menu__box')){
            $('.b-header__main__menu').toggleClass('_act');
            $('.b-container').toggleClass('_openMenu')
        }
    }
});

$(document).on('click touch', '.b-container', function(e){
    var target = $(e.target);
    if(Modernizr.touch || $(window).width() <= 640){
        if(!target.parents().hasClass('b-header')){
            $('.b-header__main__menu').removeClass('_act');
            $('.b-container').removeClass('_openMenu')
        }
    }
});

$('.b-header__main__menu__box__i').hover(function(){
    if($(this).find('.b-header__main__menu__box__i__pod-menu').length && $(window).width() > 640){
        $('.b-header__main__menu__box__modal').addClass('_act');
    }
}, function(){
    if($('.b-header__main__menu__box__modal').hasClass('_act')){
        $('.b-header__main__menu__box__modal').removeClass('_act');
    }
});

$('.b-product-page__map__sqr').on('click', function(e){
    if($(window).width() > 640){
        e.preventDefault();
    }
});

$(window).on('load resize', function(){
    var mapImg = $('.b-product-page__map__main__i');
    if($(window).width() <= 640){
        mapImg.attr('src', 'assets/img/map-small.png');
    } else {
        mapImg.attr('src', 'assets/img/map.png');
    }
});

$(window).on('load', function(){
    function minWidth(){
        var widht = 0;
        for(var i = 0, max = $('.b-header__main__menu__box__i').length; i < max; i++){
            widht += (20 + $('.b-header__main__menu__box__i').width());
        }
        if(widht > 640){
            widht = 640;
        }
        return widht
    }
    $('.b-header__main__menu__box__rasp').css({width: 100 / (4.5 - $('.b-header__main__menu__box__i').length) <= 100 ? 100 / (4.5 - $('.b-header__main__menu__box__i').length) + '%' : 100 + '%', minWidth: minWidth()});
    $('.b-collection').addClass('_act').css({bottom: $('.b-footer').outerHeight() + 5});
});

function openCategory(){
    var that = this,
        pattern = $('.b-pattern'),
        size = 0,
        timer;
    that.event = function(){
        pattern.on('click',function(e){
            var target = $(e.target);
            if(!Modernizr.touch && $(window).width() > 640 && !target.parents().hasClass('b-pattern__box__main') && !target.parents().hasClass('b-heart')){
                e.preventDefault();
                that.slideCategory($(this).index());
            } else if(target.parents().hasClass('b-heart')){
                e.preventDefault();
            }
        });

        $(document).on('click', '.b-soc-link__i', function(){
            if($(this).hasClass('_mobile')){
                !$(this).hasClass('_correct') && $('.b-product__main__modal').addClass('_visible');
            } else if($(this).hasClass('_like')){
                that.like();
            }
        });

        $(window).on('resize', function(){
            var line = $('.b-product__main__line');
            if($('.b-pattern__box__main').is(':visible')){
                $('.b-pattern__box__main').width(line.width()).css({marginLeft: line.offset().left + 5 - $('.b-pattern._act').offset().left })
            }
        });

        $(document).on('click', '.b-pattern__box__main__slider__close', function(){
            $(this).parents('.b-pattern').removeClass('_act').find('.b-pattern__box__main').slideUp('normal', function(){
                that.sliderInit($(this).parents('.b-pattern').index(), 'close');
                clearInterval(timer);
            }).find('.b-pattern__box__main__arrow').hide();
        });

        $(document).on('click', '.b-product__main__modal__close', function(){
            that.closeModal();
        });

        $(document).on('click', '.b-pattern-slider__trigger__i', function(){
            if(!$(this).hasClass('_act')){
                that.sliderPlay($(this).index());
            }
        });

        $(window).on('click', function(e){
            var target = $(e.target);
            if($('.b-product__main__modal').css('opacity') > 0 && !target.parents().hasClass('b-product__main__modal') && !target.hasClass('b-product__main__modal')){
                that.closeModal();
                e.preventDefault();
            }
        });

        $('.b-product__main__modal__form__action').on('click', function(){
            var dir = getIndex();
            function getIndex(){
                var index;
                for(var i = 0, max = pattern.length; i < max; i++){
                    if(pattern.eq(i).hasClass('_act')){
                        index = i;
                    }
                }

                return index;
            }
            if($('.b-product__main__modal__form__input').val().length > 0){
                pattern.eq(dir).find('.b-soc-link__i._mobile').addClass('_correct');
                $('.b-product__main__modal__input-correct').fadeIn('normal', function(){
                    setTimeout(function(){
                        that.closeModal();
                    }, 2000)
                })
            }
        })
    };

    that.closeModal = function(){
        $('.b-product__main__modal').removeClass('_visible');
        $('.b-product__main__modal__form__select').val($('.b-product__main__modal__form__select option:first').val());
        $('.b-product__main__modal__form__input').val('');
        setTimeout(function(){
            $('.b-product__main__modal__input-correct').hide();
        }, 500)
    };

    that.like = function(){
        var dir;
        for (var i = 0, max = pattern.length; i < max; i++){
            if(pattern.eq(i).find('.b-pattern__box__main').is(':visible')){
                dir = i;
            }
        }
        pattern.eq(dir).find('.b-soc-link__i._like').toggleClass('_act');
    };

    that.sliderAutoplay = function(){
        var actSlider, actSlid;
        for(var i = 0, max = pattern.length; i < max; i++){
            if(pattern.eq(i).find('.b-pattern__box__main').is(':visible')){
                actSlider = i;
                actSlid = pattern.eq(i).find('.b-pattern-slider__trigger__i._act').index();
            }
        }
        play();
        function play(){
            actSlid+=1;
            if(actSlid >= pattern.eq(actSlider).find('.b-pattern-slider__trigger__i').length){
                actSlid = 0;
            }
            clearInterval(timer);
            timer = setInterval(function(){
                that.sliderPlay(actSlid);
            }, 6000)
        }
    };

    that.sliderInit = function(dir, action){
        var selectBox = pattern.eq(dir),
            slider = selectBox.find('.b-pattern-slider__main'),
            slid = slider.find('img'),
            triggerBox = selectBox.find('.b-pattern-slider__trigger');
        if(action == 'open'){
            for(var i = 0, max = slid.length; i < max; i++){
                $('<div class="b-pattern-slider__trigger__i"></div>').prependTo(triggerBox);
            }
            $('.b-pattern-slider__trigger__i').eq(0).addClass('_act');
            slid.hide().eq(0).addClass('_act').show();
            that.sliderAutoplay();
        } else if(action == 'close'){
            triggerBox.html('');
            slid.removeClass('_act').show();
            clearInterval(timer);
        }
    };

    that.sliderPlay = function(dir){
        var n;

        for(var i = 0, max = pattern.length; i < max; i++){
            if(pattern.eq(i).find('.b-pattern__box__main').is(':visible')){
                n = i;
            }
        }

        var slider = pattern.eq(n).find('.b-pattern-slider__main'),
            slid = slider.find('img');

        slid.eq(dir).show();
        slider.find('.b-pattern-slider__main__pic._act').fadeOut('normal', function(){
            $(this).removeClass('_act');
            slid.eq(dir).addClass('_act');
            $('.b-pattern-slider__trigger__i._act').removeClass('_act');
            $('.b-pattern-slider__trigger__i').eq(dir).addClass('_act');
            that.sliderAutoplay();
        })


    };

    that.slideCategory = function(dir){
        var selectElem = pattern.eq(dir),
            hideBlock = $('.b-pattern__box__main'),
            arrow = selectElem.find('.b-pattern__box__main__arrow'),
            leftPos = $('.b-product__main__line').offset().left + 5 - selectElem.offset().left;

        if(selectElem.hasClass('_act')){
            selectElem.removeClass('_act').find(hideBlock).slideUp('normal', function(){
                that.sliderInit(dir, 'close');
            }).find('.b-pattern__box__main__arrow').hide();
        } else {
            if(pattern.hasClass('_act')){
                $('.b-pattern._act').removeClass('_act').find(hideBlock).slideUp('normal', function(){
                    that.sliderInit($(this).parent('.b-pattern').index(), 'close');
                    selectElem.addClass('_act').find(hideBlock).slideDown('normal', function(){
                        $(this).find('.b-pattern__box__main__arrow').show();
                    });
                    hideBlock.css({marginLeft: leftPos, width: $('.b-product__main__line').width()});
                }).find('.b-pattern__box__main__arrow').hide()
            } else {
                selectElem.addClass('_act').find(hideBlock).slideDown('normal', function(){
                    arrow.show();
                    that.sliderInit(dir, 'open');
                });
                hideBlock.css({marginLeft: leftPos, width: $('.b-product__main__line').width()});
            }
        }
    };

    that.action = function(){
        that.event();
    }();
}

$('.b-product__main__line').length && openCategory();

function openModal(){
    var that = this;
    that.event = function(){
        $('.b-header__main__search__modal').on('click', function(e){
            var target = $(e.target);
            if(target.is('.b-modal') && !Modernizr.touch){
                that.openModal();
            }

        });

        $('.b-header__main__search__box__ico').on('click', function(){
            that.openModal();
        });

        $(document).on('keyup', function(e){
            if(e.keyCode == 27 && $('.b-header__main__search__modal').is(':visible')){
                that.openModal();
            }
        });

        $(document).on('keyup mouseup', '.b-header__main__search__box__form', function(){
            var inp = $(this);
            setTimeout(function(){
                $('.b-header__main__search__modal__box').toggleClass('_act', inp.val() !== '')
            },10);
        })
    };

    that.openModal = function(){
        var inpBox = $('.b-header__main__search__box');

        $('.b-header__main__search__modal').toggleClass('_act').on('transitionend webkitTransitionEnd', function(){
            if($(this).hasClass('_act')){
                inpBox.addClass('_act');
                inpBox.find('input').focus();
            } else {
                inpBox.removeClass('_act');
                inpBox.find('input').val('');
                $('.b-header__main__search__modal__box').removeClass('_act');
            }
        });
    };

    that.action = function(){
        that.event();
    }();
}

function indexSlider(){
    var that = this,
        sliderPosition = 0,
        timeout;
    that.event = function(){
        $('.b-slider__arrow').on('click', function(){
            if($(this).hasClass('_left')){
                that.sliderPlay(-1, 'desc');
            } else if($(this).hasClass('_right')){
                that.sliderPlay(1, 'desc');
            }
        });

        $(window).on('load', function(){
            $('.b-slider__main').height($('.b-slider__main__i').height());
            $('.b-slider__main__i:not(._act)');
        });
    };

    that.mobileEvent = function(){
        $('.b-slider__main').swipe({
            swipe:function(e, dir) {
                if(dir == 'left'){
                    that.sliderPlay(1, 'mob')
                } else if(dir == 'right'){
                    that.sliderPlay(-1, 'mob')
                }
            }
        });
    };

    that.autoPlay = function(){
        timeout = setInterval(function(){
            Modernizr.touch ? that.sliderPlay(1, 'mob') : that.sliderPlay(1, 'desc');
        }, 5000)
    };

    that.sliderPlay = function(dir, device){
        var slider = $('.b-slider__main'),
            slid = $('.b-slider__main__i'),
            slidAct = $('.b-slider__main__i._act');

        if(!slid.is(':animated')){
            sliderPosition += dir;
        }

        if(sliderPosition >= slid.length){
            sliderPosition = 0;
        } else if(sliderPosition < 0){
            sliderPosition = slid.length - 1;
        }

        if(device == 'desc'){
            clearInterval(timeout);
            slid.eq(sliderPosition).addClass('_pre-act');
            slidAct.stop().fadeOut(1000, function(){
                $(this).removeClass('_act');
                $('.b-slider__main__i._pre-act').removeClass('_pre-act');
                slid.show().eq(sliderPosition).addClass('_act');
                that.autoPlay();
            })
        } else if(device == 'mob'){
            clearInterval(timeout);
            !slider.is(':animated') && slider.animate({left: - sliderPosition * 100 + '%'}, function(){that.autoPlay()});
        }

    };

    that.action = function(){
        Modernizr.touch ? that.mobileEvent() : that.event();
        that.autoPlay();
    }();
}

var modal = new openModal();

$('.b-slider').length && new indexSlider();

function Scroll(){
    $.extend(this, arguments[0] || {});
    this.init();
}
$.extend(Scroll.prototype, {
    init: function(){
        this.firstFire();
        this.events();
    },
    firstFire: function(){
        this.scroll.css({height: this.overflowAllText.outerHeight() * this.areaScroll.outerHeight() / this.allText.outerHeight()})
    },
    update: function () {
        this.scroll.css({
            top: $.proxy(function (i, val) {
                var val = parseInt(val), maxTop = this.areaScroll.outerHeight(true) - this.scroll.outerHeight(true);
                if (val < 0) {
                    return 0
                } else if (val > maxTop) {
                    return maxTop
                }
            }, this)
        });
        var val = (this.allText.outerHeight(true) - this.overflowAllText.outerHeight(true)) / (this.areaScroll.outerHeight(true) - this.scroll.outerHeight(true)) * parseInt(this.scroll.css('top'));
        this.allText.css('top', -val);
    },
    events: function(){
        this.scroll.on({
            mousedown: function(e){
                var startScrollPosition = e.pageY-parseInt($(this).css('top'));
                $(document).mousemove($.proxy(function(e){
                    $(this).css('top', e.pageY-startScrollPosition).trigger('checkAllItems');
                },this));
                return false;
            },
            'checkAllItems': $.proxy(function(){
                this.update();
            },this)
        });

        $(document).mouseup(function(){
            $(this).off('mousemove');
        });

        this.overflowAllText.on('mousewheel MozMousePixelScroll', $.proxy(function(e){
            if (e.type === 'mousewheel') {
                this.scroll.css('top',e.originalEvent.wheelDelta >= 0? '-=20': '+=20').trigger('checkAllItems');
            } else {
                this.scroll.css('top',e.originalEvent.detail <= 0? '-=20': '+=20').trigger('checkAllItems');
            }
            e.preventDefault();
        },this));
    }

});

if($('.b-specification').length && $('.b-specification__scroll__content__main').outerHeight() > $('.b-specification__scroll__content').outerHeight() && !Modernizr.touch){
    $('.b-specification__scroll').addClass('_act');
    new Scroll({
        allText: $('.b-specification__scroll__content__main'),
        overflowAllText: $('.b-specification__scroll__content'),
        areaScroll: $('.b-specification__scroll__bg'),
        scroll: $('.b-specification__scroll__bg__i')
    });
}

function prodSlider(){
    var that = this, timer,
        pics = $('.b-product-page__slider__slides').find('.b-product-page__slider__slides__pic'),
        items = [], images = [], lng = pics.length;
    that.elems = {
        massImg: pics,
        slideNum : 0,
        slideItems: $('.b-product-page__slider__slides'),
        slideNavigationWrap: $('.b-product-page__slider__navigation')
    };
    that.event = function(){
        $(document).on('click', '.b-product-page__slider__navigation__i', function(){
            var index = $(this).index();
            if(!$(this).hasClass('_act') && !$('.b-product-page__slider__slide__pic').is(':animated')){
                that.sliderMove(index, 'desc', false);
                clearInterval(timer);
                that.autoPlay();
            }
        });
    };
    that.mobileEvent = function(){
        that.elems.slideItems.swipe({
            swipe:function(e, dir) {
                if(dir == 'left'){
                    that.sliderPlay(1, 'mob', true)
                } else if(dir == 'right'){
                    that.sliderPlay(-1, 'mob', true)
                }
            }
        });
    };

    that.autoPlay = function(){
        timer = setInterval(function(){
            Modernizr.touch ? that.sliderMove(1, 'mob', true) : that.sliderMove(1, 'desc', true);
        }, 5000)
    };

    that.sliderMove = function(dir, device, auto){
        var length = that.elems.massImg.length - 1;
        if(auto){
            that.elems.slideNum += dir;
            if(that.elems.slideNum < 0){
                that.elems.slideNum = length;
            } else if(that.elems.slideNum > length){
                that.elems.slideNum = 0;
            }
        } else {
            that.elems.slideNum = dir;
        }
        $('.b-product-page__slider__slides__pic').fadeOut(1000).eq(that.elems.slideNum).fadeIn(1000);
        $('.b-product-page__slider__navigation__i').removeClass('_act').eq(that.elems.slideNum).addClass('_act');
    };

    that.action = function(){
        for(var i = 0; i<lng; i++){
            var item = $('<div class="b-product-page__slider__navigation__i"></div>');
            items.push(item);
        }
        that.elems.slideNavigationWrap.append(items);
        that.elems.slideItems.append(images);
        $('.b-product-page__slider__slides__pic').hide().eq(0).show();
        $('.b-product-page__slider__navigation__i').eq(0).addClass('_act');
        Modernizr.touch ? that.mobileEvent() : that.event();
        that.autoPlay();
        that.event();
    }();
}
$('.b-product-page__slider').length && new prodSlider();

function parallaxBg(){
    var that = this;
    that.event = function(){
        $(window).on('scroll', function(){
            that.moveBg();
        });
    }();
    that.moveBg = function(){
        var oldPos = $(document).scrollTop();
        $('.b-product-page__slider__slides__pic').css('top', (oldPos*0.3)+'px');
    };
}

$(window).on('resize load', function(){
    new parallaxBg();
});