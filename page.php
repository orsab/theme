<?php get_header(); ?>


<div class="b-main _index">
        
	<!-- Start Content -->
	<!-- TODO: delete, make other slider -->
	<?php while ( have_posts() ) : the_post(); ?>
    	<?php the_content(); ?>
    <?php endwhile;?> 

	<?php
		/**
		* Products main loop in two levels:
		* 1. Get First Product category and print
		* 2. Get 3 Products and print them
		*/

		// Get category of First product
		$args = array(
			'post_type' => 'product',
			'offset'   => 1,
		);

		$category_posts = new WP_Query($args);
	    if($category_posts->have_posts()) : 
	    	$category_posts->the_post();

		    // Get category name and description
	    	$cats = get_the_category();
			$category = $cats[0]->name;
	    	$term = get_term_by('name', $category, 'category');

	    	// Output category
	    	echo '<div class="b-collection">';
			echo '<p class="b-collection__title">'.$term->name.'</p>';
			echo '<p class="b-collection__name">'.$term->description.'</p>';
			echo '<div class="b-collection__main">';

			// Get all products
			$args = array(
				'post_type' => 'product',
				'offset'   => 0,
			);
			$products = new WP_Query($args);

			// Loop all products
	    	while($products->have_posts()) : 
	        	$products->the_post();
	        
	        	// Get Thumpnail image link
		        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
	        	echo '<div class="b-collection__main__i">';
				echo '<div class="b-collection__main__i__desc">';
				echo '<p class="b-collection__main__i__desc__review">';
				echo the_title().' '.get_field('SKU').'</p>';
				echo '<div class="b-collection__main__i__desc__heart b-heart">';
				echo '<svg version="1.1" viewBox="0 0 36 35">';
				echo '<path fill-rule="evenodd" clip-rule="evenodd" d="M27.1,2.4c-0.5-0.1-1-0.1-1.5-0.1c-3.7,0-5.5,1.6-7.6,4.1c-2.1-2.4-3.8-4.1-7.6-4.1c-0.5,0-1,0-1.5,0.1c-3.2,0.3-7,3.3-7.4,8.9v1.9C1.9,18.6,6,25.3,18,33.7c12-8.4,16.1-15.1,16.5-20.5v-1.9C34,5.6,30.3,2.7,27.1,2.4z"></path>';
				echo '</svg>';
				echo '</div>';
				echo '</div>';
				echo '<img src="'.$large_image_url[0].'" class="b-collection__main__i__img">';
				echo '</div>';
			endwhile;
			echo "</div></div>";
    	endif;
	?>
    <!-- End Content -->

</div>

<?php get_footer(); ?>